#!/bin/sh
sudo cpufreq-set -c 0 -g performance
sudo cpufreq-set -c 1 -g performance
pd extra/gulcii.pd &
pdpid=$!
sleep 5
./.cabal-sandbox/bin/gulcii 1024 768 fullscreen | pdsend 8765
kill "${pdpid}"
