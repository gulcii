GULCII
======

GULCII is an untyped lambda calculus interpreter supporting interactive
modification of a running program with graphical display of graph reduction.


Syntax
------

Lambda calculus terms with some sugar coating:
  
    term ::= variable                    -- free or bound
         | '\' variable+ strategy term   -- lambda abstraction
         | term+                         -- application
         | '(' term ')'
         | integer
         | list
    variable ::= [a-z][A-Za-z0-9]*
    strategy = '.' | '!' | '?'           -- lazy | strict | copy
    integer ::= [0-9]+                   -- uses Scott-encoding
    list ::= '[' (term (, T)*)? ']'      -- uses Scott-encoding

There are three variants of lambda abstraction: lazy, strict, and copy:

    \x . f x x    -- x is evaluated lazily with sharing
    \x ! f x x    -- x is evaluated strictly and shared
    \x ? f x x    -- x is copied before any evaluation

There is additional syntax sugar for natural numbers and lists, using
<http://en.wikipedia.org/wiki/Mogensen%E2%80%93Scott_encoding#Scott_encoding>:

    [0,1,2,3]

There is a small standard library based loosely around the Haskell Prelude:

    :load prelude
    :browse

If you define a term using free variables, they can be modified while
the program is running, but sharing is lost.  If you define a term as
a fixed point (perhaps with Y-combinator) then sharing works (to some
extent), but you can't modify the code while it is running any more.


Meta Commands
-------------

To exit type:

    :quit

Entering a term evaluates it.

Terms can be bound to names, stored in a global dictionary:

    foo = bar

The global dictionary can be listed or wiped clean:

    :browse
    :clear

Installed files can be loaded:

    :load church

Machine-readable node statistics are output to stdout, as well as when
free variables are instantiated by looking up their definitions.  These
are intended to be used for sonification, for example with Pure-data:

    pd extra/gulcii.pd &
    sleep 5
    gulcii | pdsend 8765

The sonification is controlled by two commands:

    :start
    :stop


Settings
--------

There are some runtime adjustable settings:

    :get NewsOnTop
    :set NewsOnTop
    :unset NewsOnTop
    :toggle NewsOnTop

Where NewsOnTop is a setting.  Settings include:

    TraceEvaluation
    CollectGarbage
    RealTimeDelay
    RealTimeAcceleration
    RetryIrreducible
    EmitStatistics
    EmitRebindings
    EchoToStdOut
    EchoToGUI
    SaveImages
    NewsOnTop
