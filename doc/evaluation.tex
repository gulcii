% note: much of this document is out of date

\documentclass[aspectratio=149]{beamer}
\AtBeginSection[]{\begin{frame}\frametitle{GULCII}\tableofcontents[currentsection]\end{frame}}

\usepackage{listings}

\title[GULCII]{Graphical\\ Untyped Lambda Calculus\\ Interactive Interpreter}
\subtitle{(GULCII)}
\author{Claude Heiland-Allen}
\institute{\url{https://mathr.co.uk} \\ \url{mailto:claude@mathr.co.uk}}
\date[Ed 2017]{Edinburgh, 2017}
\subject{Computer Science}

\begin{document}

\begin{frame} \titlepage
\end{frame}

\section[bruijn]{De Bruijn Indexing}

\begin{frame} \frametitle{Motivation}
\begin{itemize}
\item Naming is hard.
\item \(\alpha\)-conversion for capture-avoiding
substitution is tricky to get right.
\item De Bruijn indices solve this issue neatly.
\end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{De Bruijn Terms}
\begin{lstlisting}[language=Haskell]
data Term
  = Bound Integer
  | Lambda Term
  | Apply Term Term
\end{lstlisting}
\begin{itemize}
\item The bound variable indexes outwards to the corresponding lambda.
\end{itemize}
\[ \begin{aligned}
\text{\tt{Lambda (Lambda (Lambda (Bound {\bf 0})))}} =& \lambda x . \lambda y . \lambda \mathbf{z} . \mathbf{z} \\
\text{\tt{Lambda (Lambda (Lambda (Bound {\bf 1})))}} =& \lambda x . \lambda \mathbf{y} . \lambda z . \mathbf{y} \\
\text{\tt{Lambda (Lambda (Lambda (Bound {\bf 2})))}} =& \lambda \mathbf{x} . \lambda y . \lambda z . \mathbf{x} \\
%\text{\tt{L(A(L(A(B0)(L(B0))))(L(A(B1)(B0))))}} =&\\ \lambda z . (\lambda y . y (\lambda x . x)) (\lambda w . z w)
\end{aligned} \]
\end{frame}

\begin{frame}[fragile] \frametitle{Beta Reduction}
\begin{lstlisting}[language=Haskell]
beta :: Integer -> Term -> Term -> Term
beta i s@(Bound j) t = if i == j then t else s
beta i (Lambda s)  t = Lambda (beta (i + 1) s t)
beta i (Apply a b) t = Apply (beta i a t) (beta i b t)
\end{lstlisting}
\end{frame}

\begin{frame}[fragile] \frametitle{De Bruijn Terms in GULCII}
\begin{centering}\begin{lstlisting}[language=Haskell]
data Term
  = Free String
  | Bound Integer
  | Lambda Strategy Term
  | Apply Term Term
\end{lstlisting}\end{centering}
\begin{itemize}
\item An additional constructor for named free variables.
\item An additional strategy parameter for lambdas.
\end{itemize}
\end{frame}

\section[strategies]{Evaluation Strategies}

\begin{frame}[fragile] \frametitle{Evaluation Strategies}
\begin{lstlisting}[language=Haskell]
data Strategy
  = Lazy
  | Strict
  | Copy
\end{lstlisting}
\end{frame}

\begin{frame}[fragile] \frametitle{Copy Evaluation}
\begin{lstlisting}[language=Haskell]
y = (\ x ? A) B
\end{lstlisting}
\begin{itemize}
\item Evaluation of \(y\) proceeds by substituting \(B\) into all
      occurences of \(x\) in \(A\), before anything else is evaluated.
\item If \(B\) is not needed by the result, it will not be evaluated.
\item If \(B\) is needed by the result more than once, it will be
      evaluated more than once.
\end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{Strict Evaluation}
\begin{lstlisting}[language=Haskell]
y = (\ x ! A) B
\end{lstlisting}
\begin{itemize}
\item Evaluation of \(y\) proceeds by first completely evaluating \(B\)
      before substituting it into occurences of \(x\) in \(A\).
\item If \(B\) is not needed by the result, more work than necessary
      will be performed.
\item Syntax in GULCII inspired by Haskell's \tt{-XBangPatterns}.
\end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{Lazy Evaluation}
\begin{lstlisting}[language=Haskell]
y = (\ x . A) B
\end{lstlisting}
\begin{itemize}
\item Evaluation of \(y\) shares the evaluation of \(B\) over all
      occurences of \(x\) in \(A\), and \(B\) is evaluated on demand
      as needed.
\item If \(B\) is not needed by the result, it will not be evaluated.
\item If \(B\) is needed by the result more than once, it will be
      evaluated only once, with the result shared.
\end{itemize}
\end{frame}

\section[graph]{Graph Reduction}

\begin{frame}[fragile] \frametitle{Graph Reduction - Types}
\begin{lstlisting}[language=Haskell]
data Term
  = Free String
  | Bound Integer
  | Lambda Strategy Term
  | Apply Term Term
  | Reference Integer

type References = Map Integer Term
type Definitions = Map String Term

reduce
  :: Definitions
  -> References -> Term
  -> Maybe (References, Term)
\end{lstlisting}
\end{frame}

\begin{frame}[fragile] \frametitle{Graph Reduction - Free}
\begin{lstlisting}[language=Haskell]
reduce defs refs (Free var)
  = (,) refs `fmap` Map.lookup var defs
\end{lstlisting}
\end{frame}

\begin{frame}[fragile] \frametitle{Graph Reduction - Bound}
\begin{lstlisting}[language=Haskell]
reduce defs refs (Bound var)
  = Nothing
\end{lstlisting}
\end{frame}

\begin{frame}[fragile] \frametitle{Graph Reduction - Lambda}
\begin{lstlisting}[language=Haskell]
reduce defs refs (Lambda strat term)
  = fmap (Lambda strat) `fmap` reduce defs refs term
\end{lstlisting}
\end{frame}

\begin{frame}[fragile] \frametitle{Graph Reduction - Reference}
\begin{lstlisting}[language=Haskell]
reduce defs refs term@(Reference ref)
  = case Map.lookup ref refs of
      Just refTerm ->
        case reduce defs refs refTerm of
          Just (refs', term') ->
            Just (Map.insert ref term' refs', term)
          Nothing ->
            Just (refs, refTerm)
      Nothing -> error "reference not found"
\end{lstlisting}
\end{frame}

\begin{frame}[fragile] \frametitle{Graph Reduction - Apply Lambda Copy}
\begin{lstlisting}[language=Haskell]
reduce defs refs (Apply (Lambda Copy a) b)
  = Just (refs, beta 0 a b)

beta :: Integer -> Term -> Term -> Term
beta i s@(Bound j)  t = if i == j then t else s
beta i (Lambda k s) t = Lambda k (beta (i + 1) s t)
beta i (Apply a b)  t = Apply (beta i a t) (beta i b t)
beta i s            t = s
\end{lstlisting}
\end{frame}

\begin{frame}[fragile] \frametitle{Graph Reduction - Apply Lambda Strict}
\begin{lstlisting}[language=Haskell]
reduce defs refs (Apply l@(Lambda Strict a) b)
  = case reduce defs refs b of
      Just (refs', b') -> Just (refs', Apply l b')
      Nothing -> Just (refs, beta 0 a b)
\end{lstlisting}
\end{frame}

\begin{frame}[fragile] \frametitle{Graph Reduction - Apply Lambda Lazy}
\begin{lstlisting}[language=Haskell]
reduce defs refs (Apply (Lambda Lazy a) b)
  = let r = next refs
    in  Just ( Map.insert r b refs
              , beta 0 a (Reference r) )

next :: Map Integer v -> Integer
next refs = case Map.maxViewWithKey refs of
  Nothing          -> 0
  Just ((k, _), _) -> k + 1
\end{lstlisting}
\end{frame}

\begin{frame}[fragile] \frametitle{Graph Reduction - Apply}
\begin{lstlisting}[language=Haskell]
reduce defs refs (Apply a b)
  = case reduce defs refs a of
      Just (refs', a') ->
        Just (refs', Apply a' b)
      Nothing ->
        fmap (Apply a) `fmap` reduce defs refs b
\end{lstlisting}
\end{frame}

\section[garbage]{Garbage Collection}

\begin{frame} \frametitle{The Problems}
\begin{itemize}
\item
The References map may contain terms that are no longer referenced.

This leaks memory in the interpreter, which may be problematic for
long-running programs.

\item
There may end up a long chain of references, adding additional layers
of indirection, that will only be collapsed when the deepest term is
irreducible.

\item
We want to replace references with their terms as soon as they occur
only once.

\end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{Example Evaluation With GC Disabled}
\begin{lstlisting}[language=Haskell,basicstyle=\small,mathescape]
  zero = \s z . z
  succ = \n s z . s (n s z)
  double = \m . m succ m

  (double (succ zero))
$\to$      $\cdots$
  (\.(\.(1 ((#6 1) 0))))
   where
    #0 = (\.(\.(1 0)))
    #1 = (\.(\.0))
    #2 = 1
    #3 = 0
    #4 = (\.(\.(\.(1 ((2 1) 0)))))
    #5 = #0
    #6 = #5
$\to$      $\cdots$
\end{lstlisting}

Term $\to \#6 \to \#5 \to \#0 \to$ is a chain of indirection.

References $\#1, \#2, \#3, \#4$ are all unreachable.
\end{frame}

\begin{frame} \frametitle{Example Evaluation With GC Disabled}
\only<1>{\includegraphics[width=\linewidth]{gulcii-15}}%
\end{frame}

\begin{frame}[fragile] \frametitle{Garbage Collection}
\begin{lstlisting}[language=Haskell]
gc
  :: References -> Term
  -> (References, Term)

gc refs term
  = let counts = refCount refs term Map.empty
    in ( Map.fromList
           [ (r, compact counts refs (refs Map.! r))
           | (r, n) <- Map.toList counts
           , n > 1 ]
        , compact counts refs term )
\end{lstlisting}
\end{frame}

\begin{frame}[fragile] \frametitle{GC Helpers}
\begin{lstlisting}[language=Haskell]
type Count = Integer

refCount
  :: References -> Term
  -> Map Integer Count -> Map Integer Count

compact
  :: Map Integer Count -> References
  -> Term -> Term
\end{lstlisting}
\end{frame}

\begin{frame}[fragile] \frametitle{Reference Counting}
\begin{lstlisting}[language=Haskell]
refCount refs (Lambda strat a) counts
  = refCount refs a counts

refCount refs (Apply a b) counts
  = refCount refs a (refCount refs b counts)

refCount refs (Reference r) counts
  = case r `Map.lookup` counts of
      Just n ->
        Map.insert r (n + 1) counts
      Nothing ->
        refCount refs (refs Map.! r)
               (Map.insert r 1 counts)

refCount refs term m = m
\end{lstlisting}
\end{frame}

\begin{frame}[fragile] \frametitle{Compaction}
\begin{lstlisting}[language=Haskell]
compact counts refs term@(Reference r)
  = case r `Map.lookup` counts of
      Just 1 -> compact counts refs (refs Map.! r)
      _ -> term

compact counts refs (Lambda strat term)
  = Lambda strat (compact counts refs term)

compact counts refs (Apply a b)
  = Apply (compact counts refs a)
          (compact counts refs b)

compact counts refs term = term
\end{lstlisting}
\end{frame}

\section[examples]{Examples}

\begin{frame} \frametitle{succ zero}
\only<1>{\includegraphics[width=\linewidth]{succ-zero/gulcii-0}}%
\only<2>{\includegraphics[width=\linewidth]{succ-zero/gulcii-1}}%
\only<3>{\includegraphics[width=\linewidth]{succ-zero/gulcii-2}}%
\only<4>{\includegraphics[width=\linewidth]{succ-zero/gulcii-3}}%
\only<5>{\includegraphics[width=\linewidth]{succ-zero/gulcii-4}}%
\only<6>{\includegraphics[width=\linewidth]{succ-zero/gulcii-5}}%

apply is orange, lambda is magenta, variable is purple, free variable is cyan
\end{frame}

\begin{frame}[fragile] \frametitle{double (succ zero) - Copy}
\begin{lstlisting}[language=Haskell]
  succ = \n s z ? s (n s z)
  zero = \s z ? z
  double = \m ? m succ m
\end{lstlisting}
\end{frame}

\begin{frame} \frametitle{double (succ zero) - Copy}
\only<1>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-0}}%
\only<2>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-1}}%
\only<3>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-2}}%
\only<4>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-3}}%
\only<5>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-4}}%
\only<6>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-5}}%
\only<7>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-6}}%
\only<8>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-7}}%
\only<9>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-8}}%
\only<10>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-9}}%
\only<11>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-10}}%
\only<12>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-11}}%
\only<13>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-12}}%
\only<14>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-13}}%
\only<15>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-14}}%
\only<16>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-15}}%
\only<17>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-16}}%
\only<18>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-17}}%
\only<19>{\includegraphics[width=\linewidth]{double-(succ-zero)-copy/gulcii-18}}%

copy-lambda is pink
\end{frame}

\begin{frame}[fragile] \frametitle{double (succ zero) - Strict}
\begin{lstlisting}[language=Haskell]
  succ = \n s z ! s (n s z)
  zero = \s z ! z
  double = \m ! m succ m
\end{lstlisting}
\end{frame}

\begin{frame} \frametitle{double (succ zero) - Strict}
\only<1>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-0}}%
\only<2>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-1}}%
\only<3>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-2}}%
\only<4>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-3}}%
\only<5>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-4}}%
\only<6>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-5}}%
\only<7>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-6}}%
\only<8>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-7}}%
\only<9>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-8}}%
\only<10>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-9}}%
\only<11>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-10}}%
\only<12>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-11}}%
\only<13>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-12}}%
\only<14>{\includegraphics[width=\linewidth]{double-(succ-zero)-strict/gulcii-13}}%

strict-lambda is red
\end{frame}


\begin{frame}[fragile] \frametitle{double (succ zero) - Lazy}
\begin{lstlisting}[language=Haskell]
  succ = \n s z . s (n s z)
  zero = \s z . z
  double = \m . m succ m
\end{lstlisting}
\end{frame}

\begin{frame} \frametitle{double (succ zero) - Lazy}
\only<1>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-0}}%
\only<2>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-1}}%
\only<3>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-2}}%
\only<4>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-3}}%
\only<5>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-4}}%
\only<6>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-5}}%
\only<7>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-6}}%
\only<8>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-7}}%
\only<9>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-8}}%
\only<10>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-9}}%
\only<11>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-10}}%
\only<12>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-11}}%
\only<13>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-12}}%
\only<14>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-13}}%
\only<15>{\includegraphics[width=\linewidth]{double-(succ-zero)-lazy/gulcii-14}}%

references are green (first occurence) and turquoise (later occurences)
\end{frame}

\begin{frame}[fragile] \frametitle{Evaluation Strategy Comparison 1/3}
\begin{lstlisting}[language=Haskell]
zero = \s z @ z
succ = \n s z @ s (n s z)
double = \n @ n succ n
const = \a b @ a
join = \f a @ f a a

join const (double (succ zero))
\end{lstlisting}
\begin{itemize}
\item @ = ? Copy : 16 beta reductions, 8 free variable instantiations
\item @ = ! Strict : 13 beta reductions, 6 free variable instantiations
\item @ = . Lazy : 13 beta reductions, 6 free variable instantiations
\end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{Evaluation Strategy Comparison 2/3}
\begin{lstlisting}[language=Haskell]
id = \x @ x
loop = (\x @ x x) (\x @ x x )
const = \a b @ a

const id loop
\end{lstlisting}
\begin{itemize}
\item @ = ? Copy : 2 beta reductions, 2 free variable instantiations
\item @ = ! Strict : does not terminate
\item @ = . Lazy : 2 beta reductions, 2 free variable instantiations
\end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{Evaluation Strategy Comparison 3/3}
\begin{itemize}
\item Copy evaluation is super-easy to implement, but inefficient.

\item Strict evaluation can reduce space usage.

\item
Lazy evaluation terminates more often than strict evaluation, and is
just as efficient in terms of number of reductions.

\item In GULCII we can mix and match within a single term.
\end{itemize}

\end{frame}

\end{document}
