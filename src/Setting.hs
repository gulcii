{-
    gulcii -- graphical untyped lambda calculus interpreter
    Copyright (C) 2011, 2013, 2017  Claude Heiland-Allen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-}

module Setting (Setting(..), parse, Settings(..), get, set, defaults) where

import Control.Applicative ((<$))
import Data.Foldable (asum)

import Parse

data Setting
  = TraceEvaluation
  | CollectGarbage
  | RealTimeDelay
  | RealTimeAcceleration
  | RetryIrreducible
  | EmitStatistics
  | EmitRebindings
  | EchoToStdOut
  | EchoToGUI
  | SaveImages
  | NewsOnTop
  deriving (Eq, Ord, Enum, Bounded, Read, Show)

parse :: Parser String Setting
parse = asum [ s <$ sym (show s) | s <- [minBound .. maxBound :: Setting] ]

data Settings = Settings
  { traceEvaluation :: Bool
  , collectGarbage :: Bool
  , realTimeDelay :: Bool
  , realTimeAcceleration :: Bool
  , retryIrreducible :: Bool
  , emitStatistics :: Bool
  , emitRebindings :: Bool
  , echoToStdOut :: Bool
  , echoToGUI :: Bool
  , saveImages :: Bool
  , newsOnTop :: Bool
  }

get :: Setting -> Settings -> Bool
get TraceEvaluation = traceEvaluation
get CollectGarbage = collectGarbage
get RealTimeDelay = realTimeDelay
get RealTimeAcceleration = realTimeAcceleration
get RetryIrreducible = retryIrreducible
get EmitStatistics = emitStatistics
get EmitRebindings = emitRebindings
get EchoToStdOut = echoToStdOut
get EchoToGUI = echoToGUI
get SaveImages = saveImages
get NewsOnTop = newsOnTop

set :: Setting -> Bool -> Settings -> Settings
set TraceEvaluation b s = s{ traceEvaluation = b }
set CollectGarbage b s = s{ collectGarbage = b }
set RealTimeDelay b s = s{ realTimeDelay = b }
set RealTimeAcceleration b s = s{ realTimeAcceleration = b }
set RetryIrreducible b s = s{ retryIrreducible = b }
set EmitStatistics b s = s{ emitStatistics = b }
set EmitRebindings b s = s{ emitRebindings = b }
set EchoToStdOut b s = s{ echoToStdOut = b }
set EchoToGUI b s = s{ echoToGUI = b }
set SaveImages b s = s{ saveImages = b }
set NewsOnTop b s = s{ newsOnTop = b }

defaults :: Settings
defaults = Settings
  { traceEvaluation = False
  , collectGarbage = True
  , realTimeDelay = True
  , realTimeAcceleration = True
  , retryIrreducible = True
  , emitStatistics = True
  , emitRebindings = True
  , echoToStdOut = True
  , echoToGUI = False
  , saveImages = False
  , newsOnTop = True
  }
